import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import Header from "./components/Header";
import Home from "./components/Home";
import Lists from "./components/Lists";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Header />
        <Switch>
          <Redirect exact from="/" to="/boards" />
          <Route path="/boards" component={Home} />
          <Route path="/board/:boardID" component={Lists} />
        </Switch>
      </Router>
    );
  }
}
