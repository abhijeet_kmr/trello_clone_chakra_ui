import React, { Component } from "react";
import { Link } from "react-router-dom";
import { AddIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Text, Spinner } from "@chakra-ui/react";

import * as BoardData from "./apiCalls";
import ModalChakra from "./ModalChakra";

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boardData: [],
      isLoading: true,
      isError: false,
      modalIsOpen: false,
      boardTitle: "",
    };
  }

  componentDidMount = () => {
    BoardData.getBoards()
      .then((boardData) => {
        this.setState({
          boardData,
          isLoading: false,
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          boardData: null,
          isLoading: false,
          isError: "Failed to load the Board Data!",
        });
      });
  };

  handleCreateBoardButton = () => {
    BoardData.createBoard(this.state.boardTitle)
      .then((response) => {
        const newBoardData = [...this.state.boardData, response];
        this.setState({
          boardData: newBoardData,
          modalIsOpen: false,
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          boardData: null,
          isLoading: false,
          isError: "Failed To Create Board!",
        });
      });
  };

  handleInputChange = (e) => {
    const title = e.target.value;
    this.setState({
      boardTitle: title,
    });
  };

  onOpen = () => {
    this.setState({
      modalIsOpen: true,
    });
  };

  onClose = () => {
    this.setState({ modalIsOpen: false });
  };

  render() {
    if (this.state.isError) {
      return (
        <Flex h="80vh" justify="center" align="center">
          <Text as="h1" color="red">
            {this.state.isError}
          </Text>
        </Flex>
      );
    }

    if (this.state.isLoading) {
      return (
        <Flex h="80vh" direction="column" justify="center" align="center">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      );
    }

    return (
      <Box p="3rem 6rem" bg="#027aa0" minH="95vh">
        <Box>
          <Box>
            <Button
              onClick={this.onOpen}
              boxShadow="base"
              m="1rem"
              p="1rem"
              fontSize="1.5rem"
            >
              Create New Board <AddIcon m="0 .5rem" w={5} h={5} />
            </Button>
          </Box>

          <ModalChakra
            isOpen={this.state.modalIsOpen}
            title={"Board Title"}
            onChangeFn={this.handleInputChange}
            plaeceholder={"Enter Board Title"}
            onClickOnCloseFn={this.onClose}
            createFn={this.handleCreateBoardButton}
          />

          <Flex wrap="wrap">
            {this.state.boardData.map((board) => {
              return (
                <Link key={board.id} to={`/board/${board.id}`}>
                  <Flex
                    boxShadow="2xl"
                    border="1px dotted #fff"
                    justify="space-between"
                    bg="#026AA7"
                    m="1rem"
                    w="300px"
                    h="100px"
                    borderRadius="1rem"
                  >
                    <Text fontSize="1.5rem" color="white" p=".5rem 1rem">
                      {board.name}
                    </Text>
                  </Flex>
                </Link>
              );
            })}
          </Flex>
        </Box>
      </Box>
    );
  }
}
