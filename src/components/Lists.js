import React, { Component } from "react";
import { Link } from "react-router-dom";
import { AddIcon, DeleteIcon, ArrowBackIcon } from "@chakra-ui/icons";
import { Box, Button, Flex, Text, Spinner } from "@chakra-ui/react";

import * as BoardData from "./apiCalls";
import Cards from "./Cards";
import ModalChakra from "./ModalChakra";

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allLists: [],
      cardName: "",
      listName: "",
      isLoading: true,
      isError: false,
      isOpen: false,
      showForm: false,
    };
  }
  componentDidMount = () => {
    const idBoard = this.props.match.params.boardID;
    BoardData.getAllLists(idBoard)
      .then((allLists) => {
        this.setState({
          allLists,
          isLoading: false,
          isError: false,
        });
      })
      .catch(() => {
        this.setState({
          allLists: null,
          isLoading: false,
          isError: "Failed To Load The Lists",
        });
      });
  };

  handleCreateList = () => {
    const idBoard = this.props.match.params.boardID;
    BoardData.createNewList(this.state.listName, idBoard)
      .then((response) => {
        const newListData = [...this.state.allLists, response];
        this.setState({
          allLists: newListData,
          isOpen: false,
          showForm: false,
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          allLists: null,
          isLoading: false,
          isError: "Failed To Create List",
        });
      });
  };

  handleDeleteList = (id) => {
    BoardData.deleteList(id, true)
      .then(() => {
        const newAllListData = this.state.allLists.filter(
          (item) => item.id !== id
        );
        this.setState({
          allLists: newAllListData,
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          allLists: null,
          isLoading: false,
          isError: "Failed To Delete List",
        });
      });
  };

  handleListName = (e) => {
    const listName = e.target.value;
    this.setState({
      listName: listName,
    });
  };

  onOpen = () => {
    this.setState({
      isOpen: true,
    });
  };

  onClose = () => {
    this.setState({ isOpen: false });
  };

  render() {
    if (this.state.isError) {
      return (
        <Flex h="80vh" justify="center" align="center">
          <Text as="h1" color="red">
            {this.state.isError}
          </Text>
        </Flex>
      );
    }

    if (this.state.isLoading) {
      return (
        <Flex h="80vh" direction="column" justify="center" align="center">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      );
    }

    return (
      <Box>
        <Box bg="#027AA0">
          <Link to="/boards">
            <Button
              boxShadow="xl"
              h="2rem"
              w="6rem"
              m="1rem 0 0 2rem"
              p="0 1rem 0 0"
            >
              <ArrowBackIcon w="1.5rem" /> Back
            </Button>
          </Link>
        </Box>
        <Flex
          overflowX="scroll"
          justify="start"
          align="start"
          bg="#027AA0"
          maxW="100vw"
          minH="100vh"
          whiteSpace="nowrap"
          pb="17px"
          px="32px"
          sx={{
            "::-webkit-scrollbar": {
              display: "none",
            },
          }}
        >
          {this.state.allLists.map((list) => {
            return (
              <Flex
                key={list.id}
                direction="column"
                w="350px"
                bg="#D0D0D0"
                m="1rem 1rem 0 0"
                borderRadius="5px"
                p="1rem"
              >
                <Flex justify="space-between">
                  <Text fontWeight="bold" fontSize="2xl">
                    {list.name}
                  </Text>
                  <Button
                    onClick={() => this.handleDeleteList(list.id)}
                    fontWeight="bold"
                  >
                    <DeleteIcon />
                  </Button>
                </Flex>
                <Flex>
                  <Cards id={list.id} />
                </Flex>
              </Flex>
            );
          })}
          <Box>
            <Button
              boxShadow="base"
              m="1rem 0 0 0"
              p="1rem"
              fontSize="1.5rem"
              onClick={this.onOpen}
            >
              Create New List <AddIcon m="0 .5rem" w={5} h={5} />
            </Button>

            <ModalChakra
              isOpen={this.state.isOpen}
              title="List Title"
              placeholder="Enter List Title"
              createFn={this.handleCreateList}
              onChangeFn={this.handleListName}
              onClickOnCloseFn={this.onClose}
            />
          </Box>
        </Flex>
      </Box>
    );
  }
}
