import axios from "axios";

const APIKey = "8403dd81c6402a8cdf2550ca01400b5c";
const APIToken =
  "568ca2821e93d8df882edfdb77c501a65d6e8942af66f607c76ee3ed752eab42";

axios.defaults.baseURL = "https://api.trello.com/1/";
axios.defaults.params = {
  key: APIKey,
  token: APIToken,
};

export const getBoards = () => {
  return axios
    .get(`members/me/boards?fields=name,url&filter=open`)
    .then((res) => {
      return res.data;
    });
};

export const createBoard = (name) => {
  return axios.post(`boards/?name=${name}`).then((res) => {
    return res.data;
  });
};

export const getAllLists = (idBoard) => {
  return axios.get(`boards/${idBoard}/lists`).then((res) => {
    return res.data;
  });
};

export const createNewList = (name, idBoard) => {
  return axios
    .post(`lists?name=${name}&idBoard=${idBoard}`)
    .then((res) => res.data);
};

export const deleteList = (id, value) => {
  return axios.put(`lists/${id}/closed?value=${value}`);
};

export const getAllCards = (idList) => {
  return axios.get(`lists/${idList}/cards`).then((res) => {
    return res.data;
  });
};

export const createNewCard = (name, idList) => {
  return axios
    .post(`cards?idList=${idList}&name=${name}`)
    .then((res) => res.data);
};

export const deleteCard = (idCard) => {
  return axios.delete(`cards/${idCard}`);
};

export const getAllChecklists = (idCard) => {
  return axios.get(`cards/${idCard}/checklists`).then((res) => {
    return res.data;
  });
};

export const createCheckList = (idCard, name) => {
  return axios.post(`cards/${idCard}/checklists?&name=${name}`).then((res) => {
    return res.data;
  });
};

export const deleteCheckList = (idCheckList) => {
  return axios.delete(`checklists/${idCheckList}`);
};

export const createCheckItem = (idCheckList, name) => {
  return axios
    .post(`checklists/${idCheckList}/checkItems?name=${name}`)
    .then((res) => {
      return res.data;
    });
};

export const deleteCheckItem = (idCheckList, idCheckItem) => {
  return axios.delete(`checklists/${idCheckList}/checkItems/${idCheckItem}`);
};

export const updateItemOnCheckList = (idCard, idCheckItem, value) => {
  return axios
    .put(`cards/${idCard}/checkItem/${idCheckItem}?state=${value}`)
    .then((res) => {
      return res.data;
    });
};
