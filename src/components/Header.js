import React, { Component } from "react";
import { Box, Flex, Image, Text, Input } from "@chakra-ui/react";

export default class Header extends Component {
  render() {
    return (
      <Flex bg="#026AA7" w="100%" color="white" align="center">
        <Flex w="100%" p={4} color="white" align="center">
          <svg
            width="24"
            height="24"
            role="presentation"
            focusable="false"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M4 5C4 4.44772 4.44772 4 5 4H7C7.55228 4 8 4.44772 8 5V7C8 7.55228 7.55228 8 7 8H5C4.44772 8 4 7.55228 4 7V5ZM4 11C4 10.4477 4.44772 10 5 10H7C7.55228 10 8 10.4477 8 11V13C8 13.5523 7.55228 14 7 14H5C4.44772 14 4 13.5523 4 13V11ZM11 4C10.4477 4 10 4.44772 10 5V7C10 7.55228 10.4477 8 11 8H13C13.5523 8 14 7.55228 14 7V5C14 4.44772 13.5523 4 13 4H11ZM10 11C10 10.4477 10.4477 10 11 10H13C13.5523 10 14 10.4477 14 11V13C14 13.5523 13.5523 14 13 14H11C10.4477 14 10 13.5523 10 13V11ZM17 4C16.4477 4 16 4.44772 16 5V7C16 7.55228 16.4477 8 17 8H19C19.5523 8 20 7.55228 20 7V5C20 4.44772 19.5523 4 19 4H17ZM16 11C16 10.4477 16.4477 10 17 10H19C19.5523 10 20 10.4477 20 11V13C20 13.5523 19.5523 14 19 14H17C16.4477 14 16 13.5523 16 13V11ZM5 16C4.44772 16 4 16.4477 4 17V19C4 19.5523 4.44772 20 5 20H7C7.55228 20 8 19.5523 8 19V17C8 16.4477 7.55228 16 7 16H5ZM10 17C10 16.4477 10.4477 16 11 16H13C13.5523 16 14 16.4477 14 17V19C14 19.5523 13.5523 20 13 20H11C10.4477 20 10 19.5523 10 19V17ZM17 16C16.4477 16 16 16.4477 16 17V19C16 19.5523 16.4477 20 17 20H19C19.5523 20 20 19.5523 20 19V17C20 16.4477 19.5523 16 19 16H17Z"></path>
          </svg>

          <Image
            height="1.1rem"
            p="0 .5rem"
            src="https://a.trellocdn.com/prgb/dist/images/header-logo-spirit-loading.87e1af770a49ce8e84e3.gif"
            alt="logo"
          />
          <Text p="0 .5rem" fontWeight="bold">
            Workspaces
          </Text>

          <Text p="0 .5rem" fontWeight="bold">
            Recent
          </Text>
          <Text p="0 .5rem" fontWeight="bold">
            Starred
          </Text>
          <Text p="0 .5rem" fontWeight="bold">
            Templates
          </Text>
        </Flex>

        <Input
          h="2rem"
          w="15rem"
          type="text"
          placeholder="Search..."
          bg="#fff"
          borderRadius="1rem"
        />
        <Box
          fontSize="1.4rem"
          bg="brown"
          color="white"
          m="0 1rem"
          w="3.3rem"
          h="2.5rem"
          textAlign="center"
          border="2px solid black"
          borderRadius="50%"
        >
          AK
        </Box>
      </Flex>
    );
  }
}
