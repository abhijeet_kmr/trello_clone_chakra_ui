import React, { Component } from "react";
import {
  Button,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";

export default class ModalChakra extends Component {
  render() {
    const {
      isOpen,
      title,
      placeholder,
      createFn,
      onChangeFn,
      onClickOnCloseFn,
    } = this.props;
    return (
      <Modal isOpen={isOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{title}</ModalHeader>
          <ModalBody>
            <Input
              onChange={onChangeFn}
              type="text"
              placeholder={placeholder}
            />
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClickOnCloseFn}>
              Close
            </Button>
            <Button onClick={createFn} colorScheme="teal">
              Create
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    );
  }
}
