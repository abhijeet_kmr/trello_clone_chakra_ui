import React, { Component } from "react";
import { AddIcon, CloseIcon, DeleteIcon } from "@chakra-ui/icons";
import {
  Box,
  Button,
  Flex,
  Input,
  Text,
  Modal,
  ModalOverlay,
} from "@chakra-ui/react";

import * as BoardData from "./apiCalls";
import CheckList from "./CheckList";

export default class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allCards: [],
      isLoading: true,
      isError: false,
      showInput: false,
      showCreateButton: true,
      cardName: "",
      selectedId: null,
      cardModalVisibility: false,
      currentCardName: "",
    };
  }

  componentDidMount = () => {
    BoardData.getAllCards(this.props.id)
      .then((allCards) => {
        this.setState({
          allCards,
          isLoading: false,
          isError: false,
        });
      })
      .catch(() => {
        this.setState({
          allLists: null,
          isLoading: false,
          isError: "Failed To Load The Cards",
        });
      });
  };

  handleAddCard = () => {
    BoardData.createNewCard(this.state.cardName, this.props.id)
      .then((response) => {
        const newCardData = [...this.state.allCards, response];
        this.setState({
          allCards: newCardData,
          showInput: false,
          showCreateButton: true,
          cardName: "",
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          allCards: null,
          isLoading: false,
          isError: "Failed To Create New Card",
        });
      });
  };

  handleDeleteCard = (idCard) => {
    BoardData.deleteCard(idCard)
      .then(() => {
        const newCardData = this.state.allCards.filter(
          (item) => item.id !== idCard
        );
        this.setState({
          allCards: newCardData,
          isError: false,
        });
      })
      .catch((err) => {
        this.setState({
          allCards: null,
          isLoading: false,
          isError: "Failed To Delete Card",
        });
      });
  };

  handleCardNameChange = (e) => {
    const name = e.target.value;
    this.setState({
      cardName: name,
    });
  };

  handleInputShow = () => {
    this.setState({
      showInput: true,
      showCreateButton: false,
    });
  };

  handleCloseButton = () => {
    this.setState({
      showInput: false,
      showCreateButton: true,
    });
  };

  handleOpenModal = (id, cardName) => {
    this.setState({
      cardModalVisibility: true,
      selectedId: id,
      currentCardName: cardName,
    });
  };

  handleClose = () => {
    this.setState({
      cardModalVisibility: false,
      selectedId: null,
      currentCardName: "",
    });
  };

  render() {
    if (this.state.isError) {
      return (
        <Flex h="80vh" justify="center" align="center">
          <Text as="h1" color="red">
            {this.state.isError}
          </Text>
        </Flex>
      );
    }

    return (
      <>
        <Flex
          direction="column"
          w="100%"
          m=".5rem"
          justify="center"
          align="center"
        >
          {this.state.allCards.map((card) => {
            return (
              <React.Fragment key={card.id}>
                <Flex
                  w="100%"
                  justify="space-between"
                  align="center"
                  bg="#fff"
                  borderRadius="10px"
                  m=".5rem 0"
                >
                  <Flex
                    justify="space-between"
                    align="center"
                    w="100%"
                    bg="#fff"
                    // m=".5rem 0"
                    h="2.5rem"
                    borderRadius="10px"
                    cursor="pointer"
                    onClick={() => this.handleOpenModal(card.id, card.name)}
                  >
                    <Text fontSize="lg" p="0 .5rem">
                      {card.name}
                    </Text>
                  </Flex>
                  <DeleteIcon
                    m="0 .7rem 0 0"
                    cursor="pointer"
                    onClick={() => this.handleDeleteCard(card.id)}
                  />
                </Flex>
              </React.Fragment>
            );
          })}

          {this.state.showCreateButton ? (
            <Button
              onClick={this.handleInputShow}
              boxShadow="base"
              m="1rem"
              p="1rem"
              fontSize="1rem"
            >
              <AddIcon m="0 .5rem" w={4} h={4} /> Create New Card
            </Button>
          ) : null}

          {this.state.showInput ? (
            <Box>
              <Input
                onChange={this.handleCardNameChange}
                placeholder="Enter Card Name"
                border="2px"
              ></Input>

              <Flex justify="center" align="center">
                <Button m="1rem" onClick={this.handleAddCard}>
                  Add Card
                </Button>
                <CloseIcon
                  m="1rem"
                  cursor="pointer"
                  onClick={this.handleCloseButton}
                />
              </Flex>
            </Box>
          ) : null}
        </Flex>
        <Modal isOpen={this.state.cardModalVisibility} size="xl">
          <ModalOverlay />
          <CheckList
            id={this.state.selectedId}
            cardName={this.state.currentCardName}
            handleClose={this.handleClose}
          />
        </Modal>
      </>
    );
  }
}
